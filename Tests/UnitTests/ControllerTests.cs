﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using UsersApi.Controllers;
using UsersApi.DataAccess;
using UsersApi.Domain;
using UsersApi.Mappers;
using DtoUser = UsersApi.Dto.User;

namespace Tests.UnitTests
{
    public class ControllerTests
    {
        private readonly Mock<IRepository<User>> _mockRepository;
        private readonly UserController _controller;
        private readonly List<User> _users;

        public ControllerTests()
        {
            _mockRepository = new Mock<IRepository<User>>();
            var profile = new MapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(profile));
            var mapper = new Mapper(configuration);
            _controller = new UserController(_mockRepository.Object, mapper);
            _users = new List<User>
            {
                new User{ Id = 1, FirstName = "Eeter", LastName = "Peeter"},
                new User {Id = 2, FirstName = "Peter", LastName = "Peeter"}
            };
        }

        [Test]
        public void It_should_get_users()
        {
            
            var taskCompletion = new TaskCompletionSource<IEnumerable<User>>();
            taskCompletion.SetResult(_users);
            _mockRepository.Setup(r => r.AllAsync()).Returns(taskCompletion.Task);
            var result = _controller.Get();
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
            var items = (result.Result as OkObjectResult)?.Value;
            Assert.IsInstanceOf<IEnumerable<UsersApi.Dto.User>>(items);
            var itemCount = (items as IEnumerable<UsersApi.Dto.User>)?.Count();
            Assert.AreEqual(2, itemCount);
        }

        [Test]
        public void It_should_get_user_by_id()
        {
            var taskCompletion = new TaskCompletionSource<User>();
            taskCompletion.SetResult(_users.First());
            _mockRepository.Setup(r => r.FindAsync(It.IsAny<int>())).Returns(taskCompletion.Task);
            var result = _controller.Get(1);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }
        
        [Test]
        public void It_should_return_bad_request_for_get_by_id()
        {
            var taskCompletion = new TaskCompletionSource<User>();
            taskCompletion.SetResult(null);
            _mockRepository.Setup(r => r.FindAsync(It.IsAny<int>())).Returns(taskCompletion.Task);
            var result = _controller.Get(1);
            Assert.IsInstanceOf<BadRequestObjectResult>(result.Result);
        }

        [Test]
        public void It_should_post_user()
        {
            _controller.Post(new DtoUser());
            _mockRepository.Verify(m => m.InsertAsync(It.IsAny<User>()), Times.Once);
        }

        [Test]
        public void It_should_put_user()
        {
            var taskCompletion = new TaskCompletionSource<User>();
            taskCompletion.SetResult(_users.First());
            _mockRepository.Setup(r => r.FindAsync(It.IsAny<int>())).Returns(taskCompletion.Task);
            _controller.Put(1, new DtoUser());
            _mockRepository.Verify(m => m.UpdateAsync(It.IsAny<User>()), Times.Once);
        }

        [Test]
        public void It_should_delete_user()
        {
            var taskCompletion = new TaskCompletionSource<User>();
            taskCompletion.SetResult(_users.First());
            _mockRepository.Setup(r => r.FindAsync(It.IsAny<int>())).Returns(taskCompletion.Task);
            _controller.Delete(1);
            _mockRepository.Verify(m => m.RemoveAsync(It.IsAny<User>()), Times.Once);
        }
    }
}