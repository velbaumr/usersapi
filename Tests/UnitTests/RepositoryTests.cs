﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using UsersApi.DataAccess;
using UsersApi.Domain;

namespace Tests.UnitTests
{
    [TestFixture]
    public class RepositoryTests
    {
        private readonly User _user;

        public RepositoryTests()
        {
            _user = new User
            {
                FirstName = "Peeter",
                LastName = "Eeter",
                PhoneNumber = "+372 6666666",
                Email = "test@test.com"
            };
        }

        private static UserContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<UserContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString("N")).Options;
            var dbContext = new UserContext(options);
            return dbContext;
        }

        [Test]
        public async Task It_should_add_user()
        {
            await using var context = CreateDbContext();
            var repository = new Repository<User>(context);

            await repository.InsertAsync(_user);
            Assert.IsTrue(context.User.Any());
        }

        [Test]
        public async Task It_should_update_user()
        {
            await using var context = CreateDbContext();
            const string newFirstName = "Meeter";
            var repository = new Repository<User>(context);

            var user = await repository.InsertAsync(_user);
            user.FirstName = newFirstName;
            await repository.UpdateAsync(user);
            Assert.IsTrue(context.User.First().FirstName.Equals(newFirstName));
        }

        [Test]
        public async Task It_should_delete_user()
        {
            await using var context = CreateDbContext();
            var repository = new Repository<User>(context);

            var user = await repository.InsertAsync(_user);
            await repository.RemoveAsync(user);
            Assert.IsTrue(!context.User.Any());
        }
    }
}