﻿using AutoMapper;
using UsersApi.Dto;

namespace UsersApi.Mappers
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Domain.User, User>()
                .ReverseMap();
        }
    }
}