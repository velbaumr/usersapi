using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UsersApi.DataAccess;
using User = UsersApi.Dto.User;

namespace UsersApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IRepository<Domain.User> _repository;
        private readonly IMapper _mapper;

        public UserController(IRepository<Domain.User> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var users = await _repository.AllAsync();
            return Ok(_mapper.Map<IEnumerable<Domain.User>, IEnumerable<User>>(users));
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var user = await _repository.FindAsync(id);

            return user == null ? BadRequest("Not found") : Ok(_mapper.Map<Domain.User, User>(user));
        }

        [HttpPost]
        public async Task<IActionResult> Post(User user)
        {
            var domainUser = new Domain.User();
            MapUser(user, domainUser);
            try
            {
                var result =await _repository.InsertAsync(domainUser);
                return Ok(_mapper.Map<Domain.User, User>(result));
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException?.Message);
            }
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put(int id, User user)
        {
            var domainUser = await _repository.FindAsync(id);
            if (domainUser == null) return BadRequest("Not found");
            MapUser(user, domainUser);
            try
            {
                await _repository.UpdateAsync(domainUser);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException?.Message);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = await _repository.FindAsync(id);
            if (user == null) return BadRequest("Not found");

            await _repository.RemoveAsync(user);

            return Ok();
        }

        private void MapUser(User user, Domain.User domainUser)
        {
            domainUser.FirstName = user.FirstName;
            domainUser.LastName = user.LastName;
            domainUser.Email = user.Email;
            domainUser.PhoneNumber = user.PhoneNumber;
        }
    }
}