﻿using System.ComponentModel.DataAnnotations;

namespace UsersApi.Dto
{
    public class User
    {
        public int? Id { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [StringLength(10, MinimumLength = 2)]
        [Required]
        public string FirstName { get; set; }

        [StringLength(20, MinimumLength = 2)]
        [Required]
        public string LastName { get; set; }
        
        [RegularExpression(@"\+[0-9]{1,3}\ [0-9]{5,10}?$", ErrorMessage = "Phone number should be in form of +countrycode number")]
        [Required]
        public string PhoneNumber { get; set; }
    }
}