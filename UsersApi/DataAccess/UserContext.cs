﻿using Microsoft.EntityFrameworkCore;
using UsersApi.Domain;

namespace UsersApi.DataAccess
{
        public class UserContext : DbContext
        {
                public UserContext(DbContextOptions options) : base(options)
                {
                }
                
                public DbSet<User> User { get; set; }
                
                protected override void OnModelCreating(ModelBuilder modelBuilder)
                {
                        modelBuilder.Entity<User>()
                                .HasIndex(u => u.Email)
                                .IsUnique();
                        
                        modelBuilder.Entity<User>()
                                .HasIndex(u => u.PhoneNumber)
                                .IsUnique();
                        
                        base.OnModelCreating(modelBuilder);
                }
        }
}