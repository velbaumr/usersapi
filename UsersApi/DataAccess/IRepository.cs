﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UsersApi.Domain;

namespace UsersApi.DataAccess
{
    public interface IRepository<T> where T: Entity
    {
        Task<IEnumerable<T>> AllAsync();
        Task<T> FindAsync(int id);
        Task<T> InsertAsync(T entity);
        Task UpdateAsync(T entity);
        Task RemoveAsync(T entity);
    }
}